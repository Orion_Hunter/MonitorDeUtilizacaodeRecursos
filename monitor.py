import psutil
from datetime import datetime
import time
import sys

def generateFile(l):
  title = datetime.now().strftime("%d-%m-%y")
  fi = open("re"+title,"w")
  fi.write("HOUR, USER_TIME, SYSTEM_TIME, IDLE_TIME, PERCENT, FREQ")
  for r in l:
   s = str(r["Hour"])+", "+ str(r["user_time"])+", "+str(r["system_time"])+", "+str(r["idle_time"])+", "+str(r["percent"])+", "+str(r["freq"])
   fi.write("\n"+s)
  fi.close()  
   

l = []
marker = sys.argv[1]

while True:
  d = {}
  t = datetime.now()
  cpu = psutil.cpu_times()
  p = psutil.cpu_percent()
  f = psutil.cpu_freq()
  d["Hour"] = t.strftime("%H:%M:%S")
  d["user_time"] = cpu[0]
  d["system_time"]=cpu[2]
  d["idle_time"] = cpu[3]
  d["percent"] = p
  d["freq"] = f[0]
  l.append(d)
  if(t.strftime("%H:%M:%S") == str(marker)):
     generateFile(l)
     break
  time.sleep(1)
  
   
#Hour    user_time  system_time   idle_time  perccent cpu_freq
